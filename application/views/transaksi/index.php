<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo site_url('C_dashboard'); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">List Data Transaksi</li>
        </ol>
 <form>
  	<div class="form-row">
    		<div class="form-group col-md-3">
      			<a href="<?php echo base_url('index.php/C_transaksi/tambah'); ?>" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Tambah Transaksi</a>
    		</div>
  			
  		<div class="form-row">
    		<div class="form-group col-md-2">
      			<input type="date" class="form-control" id="inputCity">
    		</div>
    	<p>&nbsp; to &nbsp;</p>
    	<div class="form-row">
    		<div class="form-group col-md-3">
      			<input type="date" class="form-control" id="inputCity">
    		</div>
    	<div class="form-group col-md-3">
      		<select id="inputState" class="form-control">
        		<option selected>Choose...</option>
        		<option>...</option>
      		</select>
    	</div>
    	<div class="form-group col-md-3">
      		<input type="text" placeholder="Search..." class="form-control" id="inputZip">
    	</div>
    	<div class="form-group col-md-3">
    	<button type="submit" class="btn btn-primary">Search</button>
    </div>
  </div>
</div>
</div>
</form>
  		<br><br>
  		<div class="card mb-3">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered"  width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Deskripsi</th>
                    <th>Code</th>
                    <th>Rate euro</th>
                    <th>Date paid</th>
                    <th>Kategori</th>
                    <th>Nama Transaksi</th>
                    <th>Nominal(IDR)</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  $no = 1; foreach ( $varTransaksi as $u) : ?>
                  <tr>
                    <td><?= $no; ?></td>
                    <td><?= $u['deskripsi']; ?></td>
                    <td><?= $u['kode_transaksi']; ?></td>
                    <td><?= $u['rate_euro']; ?></td>
                    <td><?= $u['date_paid']; ?></td>
                    <td><?= $u['kategori']; ?></td>
                    <td><?= $u['nama_transaksi']; ?></td>
                    <td><?= $u['nominal']; ?></td>
                    <td>
                      <center>
                        <a href="<?php echo base_url(); ?>C_transaksi/hapus/<?= $u['kode_transaksi'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center>
                    </td>
                  </tr>
                  <?php $no++; ?>
                    <?php endforeach; ?>
              </tbody>
          </table>
      </div>
  </div>
</div>
