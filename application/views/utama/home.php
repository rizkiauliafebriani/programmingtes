<nav class="navbar navbar-expand-lg navbar-light bg-light" >
  <a class="navbar-brand" href="#">Partner Iklan.com</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown" style="padding-left: 700px;">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Homepage <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">News</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Produk
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Google</a>
          <a class="dropdown-item" href="#">Facebook Adds</a>
          <a class="dropdown-item" href="#">SEO</a>
          <a class="dropdown-item" href="#">Training</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pemesanan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Kontak</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('C_login'); ?>">Login</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="jumbotron jumbotron-fluid">
    <img src="assets/img/01.png" style="width: 1650px; height: 350px; padding-right: 300px;">
</div>
<div class="row">
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h2 class="card-title" align="center">Google AdWords</h2>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h2 class="card-title" align="center">Facebook Adds</h2>
      </div>
    </div>
  </div>

<div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h2 class="card-title" align="center">SEO</h2>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h2 class="card-title" align="center">Training</h2>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="media">
  <img src="assets/img/02.jpg" class="mr-3" alt="...">
  <div class="media-body">
    <h5 class="mt-0">Google AdWords</h5>
    <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <a href="">Read More...</a>
  </div>
</div>
<br><br>
<div class="media">
  <div class="media-body">
    <h5 class="mt-0 mb-1">Facebooks Adds</h5>
    <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <a href="">Read More...</a>
  </div>
  <img src="assets/img/02.jpg" class="ml-3" alt="...">
</div>
<br><br>
<div class="media">
  <img src="assets/img/02.jpg" class="mr-3" alt="...">
  <div class="media-body">
    <h5 class="mt-0">SEO</h5>
    <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <a href="">Read More...</a>
  </div>
</div>
<br><br>
<div class="media">
  <div class="media-body">
    <h5 class="mt-0 mb-1">Training</h5>
    <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <a href="">Read More...</a>
  </div>
  <img src="assets/img/02.jpg" class="ml-3" alt="...">
</div>
<br><br>