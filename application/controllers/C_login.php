<?php
class C_login extends CI_Controller {
	public function index()
	{

		$this->load->view('utama/head');
		$this->load->view('login/index');
	}

	public function proses_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password );
		$cek = $this->M_login->cek_login('tb_login',$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'username' => $username
				//'status' => "login"
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("index.php/C_dashboard"));

		}else{
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! username / Pasword anda tidak terdaftar</div>');
			redirect(base_url("index.php/C_login"));
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/C_login'));
	}
}