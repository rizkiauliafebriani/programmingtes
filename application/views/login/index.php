<html>
<body>
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-sm-5 p-4">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header"> 
                <center><h2>Login</h2></center>
               <hr>      
              </div>
              <div class="card-body p-6">
              <form method="POST" action="<?php echo site_url('C_login/proses_login'); ?>" id="formlogin">
                <div class="form-group">
                  <input type="text" class="form-control" id="username" name="username" placeholder="Username"  onclick='validasi("username","USERNAME")' required maxlength="20" <?php echo $this->session->flashdata('username'); ?>>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="password" class="form-control" name="password" placeholder="Password" id="password" onclick="validasi('password','PASSWORD')" required maxlength="20" <?php echo $this->session->flashdata('username'); ?>>   
                    </div>
                  </div>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Login</button>
                <a href="<?php echo site_url('C_utama'); ?>" class="btn btn-lg btn-danger btn-block">Kembali Halaman Utama</a>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
  function ShowPassword()
{
  if(document.getElementById("password").value!="")
  {
    document.getElementById("password").type="text";
    document.getElementById("show").style.display="none";
    document.getElementById("hide").style.display="block";
  }
}
 
function HidePassword()
{
  if(document.getElementById("password").type == "text")
  {
    document.getElementById("password").type="password"
    document.getElementById("show").style.display="block";
    document.getElementById("hide").style.display="none";
  }
}
</script>

<style>
  .container input[type=button]{background: blue;
    border: 0 none;
    color: white;
    font-weight: bold;
    padding: 3px;
    width: 90px;}
</style>

<script>

    var form = document.querySelector("#formlogin");

    function validasi(textbox, text) {
        var input = document.getElementById(textbox);

        var cek = form.checkValidity()
        if (cek == false) {
            input.oninvalid = function(e) {
                if (e.target.validity.valueMissing) {
                    e.target.setCustomValidity(text + " TIDAK BOLEH KOSONG");
                    return;
                }
            }
            input.oninput = function(e) {
                e.target.setCustomValidity("")
            }
            form.reportValidity();
            console.log(cek);
        }

    }
  </script>
  <style> #file_error{color: #FF0000} </style>


</body>
</html>