<?php

class C_transaksi Extends CI_Controller{

	public function index()
	{
		$data['varTransaksi']= $this->M_transaksi->getTransaksi();
		$this->load->view('template/sidebar');
		$this->load->view('transaksi/index',$data);
		$this->load->view('template/footer');		
	}

	public function tambah(){
		$this->load->view('templates/sidebar');
		$this->load->view('transaksi/tambah');
		$this->load->view('templates/footer');	
	}

	public function storePost()
    {
        if(!empty($this->input->post('addmore'))){

            foreach ($this->input->post('addmore') as $key => $value) {
                $this->db->insert('tb_transaksi',$value);
            }
            
        }

        print_r('Data Transaksi Berhasil Ditambah.');
    }
}