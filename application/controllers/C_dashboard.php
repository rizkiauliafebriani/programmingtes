<?php

class C_dashboard Extends CI_Controller{

	public function index()
	{
		$dt['varPelogin']= $this->M_dashboard->tampil_data();
		$this->load->view('template/sidebar',$dt);
		$this->load->view('dashboard/index');
		$this->load->view('template/footer');		
	}
}