<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<div id="content-wrapper">
  <div class="container-fluid">
  <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?php echo site_url('C_dashboard'); ?>">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Input Data Transaksi</li>
    </ol>
<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom01">Description</label>
      <textarea class="form-control" name="deskripsi" style="height: 100px;"></textarea>
    </div>
    <div class="col-md-6 mb-3">
      <label for="validationCustom02">Code</label>
      <input type="text" class="form-control" name="kode_transaksi">
    </div>
  </div>
  <div class="col-md-6 mb-3">
      <label for="validationCustom02" style="margin-left: 545px;">Rate</label>
      <input type="text" class="form-control" name="kode_transaksi" style="margin-left: 545px;">
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <!-- <label for="validationCustom03">City</label> -->
      <input type="hidden" class="form-control" id="validationCustom03" required>
    </div>
    <div class="col-md-5 mb-3">
      <label for="validationCustom04" style="margin-left: 5px;">Date</label>
      <input type="date" class="form-control" name="date" style="margin-left: 5px;" >
    </div>
</form>
<div id="content-wrapper">
  <div class="container-fluid">
  <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Data Transaksi</li>
    </ol>
<form name="add_name" method="POST" action="/add-more-post">
  <div class="table-responsive">  
    <table class="table table-bordered" id="dynamic_field">  
      <tr>  
        <td>
          <select class="form-control">
            <option>Category</option>
            <option value="income">Income</option>
            <option value="expense">Expense</option>
          </select>
        </td>    
      </tr>
      <tr>  
        <td align="center">Nama Transaksi</td>  
        <td align="center">Nominal(IDR)</td> 
        <td><button type="button" name="add" id="add" class="btn btn-success">+</button></td>  
      </tr>  
    </table>  
      <input type="submit" name="submit" id="submit" class="btn btn-info" value="Simpan" />
      <input type="submit" name="submit" id="submit" class="btn btn-danger" value="Batal" /> 
  </div>
</form>  
</div> 
</div>

<script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="addmore[][transaksi]" placeholder="Masukkan Nama Transaksi" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>'); 
      });
      $('#add').click(function(){  
           i++; 
      $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="addmore[][nominal]" placeholder="Masukkan Nama Nominal" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });

      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


    });  
</script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>