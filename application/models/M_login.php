<?php


class M_login extends CI_Model {
	function cek_login($table,$where){		
		return $this->db->	get_where($table,$where);
	}

	public function getLogin ()
	{
		$this->db->select('*');
		$this->db->from('tb_login');
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
		$query = $this->db->get_where();
		return $query->row_array();
	}

}